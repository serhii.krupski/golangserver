package handlers

import (
        "net/http"
        "net/http/httptest"
        "testing"
)


func TestGetUsers( t *testing.T){
   req , _ := http.NewRequest("GET", "/users", nil)
   rr := httptest.NewRecorder()
   
  http.HandlerFunc(GetUsers).ServeHTTP(rr, req)
   
  if status := rr.Code; status != http.StatusOK {
           t.Errorf("want %b, got %b", http.StatusOK, status)
   }

}
