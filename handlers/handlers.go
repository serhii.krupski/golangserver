package handlers

import (
	"encoding/json"
	"net/http"
)


type User struct {
    Name      string `json:"name"`
    BirthYear int    `json:"birth_year`
}

func GetUsers(w http.ResponseWriter, r *http.Request) {
	users := []User{
		{"Serhii", 1972},
		{"Billy", 1969},
	}

	res, _ := json.Marshal(&users)

    w.Header().Set("Content-Type", "application/json")
    w.WriteHeader(http.StatusOK)
	_, _ = w.Write(res)
}
