FROM golang:1.15.4-alpine3.12

WORKDIR  /app
COPY . ./
RUN go mod download
RUN go build

EXPOSE 8080
CMD ["./golangserver"]
