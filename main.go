package main

import (
	"log"
	"net/http"

	"github.com/gorilla/mux"

    "gitlab.com/serhii.krupski/golangserver/handlers"
)

func main() {
	r := mux.NewRouter()

	r.HandleFunc("/users", handlers.GetUsers).Methods("GET")

	s := &http.Server{
		Addr:    ":8080",
		Handler: r,
	}

	log.Fatal(s.ListenAndServe())

}
